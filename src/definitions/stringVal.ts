import { Either } from 'funfix-core';
import { nonEmpty } from '../util';

export class StringVal {
  /**
   *
   * @param js { String: { str: 'u' } }
   */
  public static fromJs(js: any): Either<Error, StringVal> {
    return nonEmpty(() => js && js.String && js.String.str)
      .map((s) => new StringVal(s));
  }

  constructor(public value: string) { }
}
