import { Either, Try } from 'funfix-core';
import { nonEmpty } from '../util';

export class AStar {
  /**
   *
   * @param js {"A_Star":{}}
   */
  public static fromJs(js: any): Either<Error, AStar> {
    return nonEmpty(() => js && js.A_Star, 'A_Star')
      .map(() => new AStar());
  }

  constructor() { }
}
