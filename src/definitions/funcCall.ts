import { StringVal } from './stringVal';
import { AConst } from './aConst';
import { Either, Try } from 'funfix-core';
import { isArray } from 'util';
import { nonEmpty } from '../util';

export class FuncCall {
  /**
   *
   * @param js {
                            'FuncCall': {
                              'funcname': StringVal[],
                              'args': AConst[]
                            }
                          }
   */
  public static fromJs(js: any): Either<Error, FuncCall> {
    return nonEmpty(() => js && js.FuncCall && [js.FuncCall.funcname, js.FuncCall.args])
      .flatMap(([funcname, args]) => {
        const nameE = (isArray(funcname) ? Either.right<Error, any[]>(funcname) : Either.left<Error, any[]>(new Error('Expected FuncCall.funcname to be an array')))
          .flatMap((name) => name.map((n) => StringVal.fromJs(n))
            .reduce<Either<Error, StringVal[]>>(
            (prev, cur) => prev.flatMap((list) => cur.map((c) => list.concat(c))), Either.right([])
            ));
        const argsE = (isArray(args) ? Either.right<Error, any[]>(args) : Either.left<Error, any[]>(new Error('Expected FuncCall.args to be an array')))
          .flatMap((_) => _.map((arg) => AConst.fromJs(arg))
            .reduce<Either<Error, AConst[]>>(
            (prev, cur) => prev.flatMap((list) => cur.map((c) => list.concat(c))),
            Either.right([])
            ));
        return nameE.flatMap((name) => argsE.map((args) => new FuncCall(name, args)));
      });
  }

  constructor(public name: StringVal[], public args: AConst[]) { }
}
