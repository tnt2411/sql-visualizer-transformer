import { StringVal } from './stringVal';
import { Try, Either } from 'funfix-core';
import { isArray } from 'util';

export class TypeName {
  /**
   *
   * @param js {
                                'TypeName': {
                                  'names': [
                                    {
                                      'String': {
                                        'str': 'date'
                                      }
                                    }
                                  ],
                                  'typemod': -1,
                                  'location': 47
                                }
                              }
   */
  public static fromJs(js: any): Either<Error, TypeName> {
    return Try.of(() => js && js.TypeName && js.TypeName.names)
      .fold(() => Either.left(new Error('Expected nonempty TypeName.names field')), (names) => Either.right(names))
      .flatMap<any[]>((names) => isArray(names) ? Either.right(names) : Either.left(new Error('Expected TypeName.names to be an array')))
      .flatMap((names) => names.map((name) => StringVal.fromJs(name))
        .reduce<Either<Error, StringVal[]>>(
        (prev, cur) => prev.flatMap((list) => cur.map((c) => list.concat(c))),
        Either.right([])))
      .map((names) => new TypeName(names));
  }

  constructor(public names: StringVal[]) { }
}
