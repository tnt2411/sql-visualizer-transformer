import { Either, Option } from 'funfix-core';
import { RangeVar } from './rangeVar';
import { isArray } from 'util';

export type FromClauseMember = RangeVar; // RangeVar | RangeFunction;

export class FromClause {
  /**
   *
   * @param value [FromClauseMember...]
   */
  public static fromJs(value: any): Either<Error, FromClause> {
    return Option.of(value)
      .fold<Either<Error, any>>(() => Either.left(new Error('Got empty value for FromClause')), (v) => Either.right(v))
      .flatMap<any[]>((v) => isArray(v) ? Either.right(v) : Either.left(new Error('Expected FromClause to be an array')))
      .flatMap((list) => {
        const parsers: Array<(_: any) => Either<Error, FromClauseMember>> = [
          RangeVar.fromJs
        ];
        return list.map((member) => parsers.reduce<Either<Error, FromClauseMember>>((prev, cur) => prev.isRight() ? prev : cur(member), Either.left(new Error('A FromClause member is not a FromClauseMember'))))
          .reduce((prev, cur) => {
            return prev.flatMap((members) => cur.map((member) => members.concat(member)))
          }, Either.right<Error, FromClauseMember[]>([]));
      })
      .map((members) => new FromClause(members));
  }

  constructor(public members: FromClauseMember[]) { }
}
