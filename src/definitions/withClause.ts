import { Either, Try } from 'funfix-core';
import { CommonTableExpr } from './commonTableExpr';
import { isArray } from 'util';

export class WithClause {
  public static fromJs(js: any): Either<Error, WithClause> {
    return Try.of(() => js && js.WithClause && js.WithClause.ctes)
      .fold(() => Either.left(new Error('Expeted nonempty WithClause.ctes')), (ctes) => Either.right(ctes))
      .flatMap<any[]>((ctes) => isArray(ctes) ? Either.right(ctes) : Either.left(new Error('Expected WithClause.ctes to be an array')))
      .flatMap((ctes) => ctes.map((cte) => CommonTableExpr.fromJs(cte))
        .reduce<Either<Error, CommonTableExpr[]>>(
        (prev, cur) => prev.flatMap((cs) => cur.map((cte) => cs.concat(cte))),
        Either.right([])))
      .map((ctes) => new WithClause(ctes));

  }

  constructor(public ctes: CommonTableExpr[]) { }
}
