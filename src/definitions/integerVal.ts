import { Either, Try } from 'funfix-core';

export class IntegerVal {
  /**
   *
   * @param js {
                                      'Integer': {
                                        'ival': number;
                                      }
                                    }
   */
  public static fromJs(js: any): Either<Error, IntegerVal> {
    return Try.of(() => js && js.Integer && js.Integer.ival)
      .fold(() => Either.left(new Error('Expected nonempty Integer.ival field')), (_) => Either.right(_))
      .map((_) => new IntegerVal(_));
  }

  constructor(public value: number) { }
}
