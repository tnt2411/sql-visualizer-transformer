import { Either, Try } from 'funfix-core';
import { Query } from './query';

export class CommonTableExpr {
  public static fromJs(js: any): Either<Error, CommonTableExpr> {
    return Try.of(() => js && js.CommonTableExpr)
      .fold(() => Either.left(new Error('Missing CommonTableExpr field')), (cte) => Either.right(cte))
      .flatMap((cte) => {
        const nameE: Either<Error, string> = cte.ctename ? Either.right(cte.ctename) : Either.left(new Error('Missing CommonTableExpr.ctename field'));
        const queryE = Query.fromJs(js.CommonTableExpr.ctequery);
        return nameE.flatMap((name) => queryE.map((query) => new CommonTableExpr(name, query)));
      });
  }

  constructor(public name: string, public query: Query) { }
}
