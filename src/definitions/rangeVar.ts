import { Option, Either, Try } from 'funfix-core';
import { Alias } from './alias';

/**
  export interface RangeVar {
  RangeVar: {
    relname: string;
    inhOpt: number;
    relpersistence: string;
    location: number;
    alias?: {
      Alias: {
        aliasname: string;
      };
    };
  };
}
 */

export class RangeVar {
  public static fromJs(js: any): Either<Error, RangeVar> {
    return Try.of(() => js && js.RangeVar)
      .fold(
      () => Either.left(new Error('Expected nonempty RangeVar field')),
      (range) => Either.right(range))
      .flatMap((range) => {
        const nameE: Either<Error, string> = range.relname ? Either.right(range.relname) : Either.left(new Error('Expected nonempty RangeVar.relname'));
        return nameE.map((name) => new RangeVar(name, Alias.fromJs(range.alias).toOption()));
      });
  }

  constructor(public relname: string, public alias: Option<Alias> = Option.none()) { }
}
