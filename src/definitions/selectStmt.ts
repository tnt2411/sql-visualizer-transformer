import { Statement } from './statement';
import { Either, Option, Try } from 'funfix-core';
import { ResTarget } from './resTarget';
import { isArray } from 'util';
import { FromClause } from './fromClause';
import { WithClause } from './withClause';

export class SelectStmt extends Statement {
  public static fromJs(js): Either<Error, SelectStmt> {
    return SelectStmt.targetListFromJs(js)
      .flatMap((targetList) => SelectStmt.fromClauseFromJs(js)
        .flatMap((fromClause) => SelectStmt.withClauseFromJs(js)
          .map((withClause) => new SelectStmt(targetList, fromClause, withClause))));
  }

  private static targetListFromJs(js): Either<Error, ResTarget[]> {
    return Try.of(() => js && js.targetList)
      .fold(() => Either.left(new Error('Expected nonempty targetList field')), (list) => Either.right(list))
      .flatMap((list) => isArray(list) ? Either.right<Error, any[]>(list) : Either.left<Error, any[]>(new Error('Expected targetList to be an array')))
      .flatMap((list) => {
        return list.map((target) => ResTarget.fromJs(target))
          .reduce<Either<Error, ResTarget[]>>(
          (prev, cur) => prev.flatMap((list) => cur.map((c) => list.concat(c))),
          Either.right([]));
      });
  }

  private static fromClauseFromJs(js: any): Either<Error, Option<FromClause>> {
    return js.fromClause ? FromClause.fromJs(js.fromClause).map((c) => Option.some(c)) : Either.right(Option.none());
  }

  private static withClauseFromJs(js: any): Either<Error, Option<WithClause>> {
    return js.withClause ? WithClause.fromJs(js.withClause).map((c) => Option.some(c)) : Either.right(Option.none());
  }

  constructor(
    public targetList: ResTarget[],
    // op: number,
    public fromClause: Option<FromClause> = Option.none(),
    // groupClause: Option<A_Const[]> = Option.none(),
    // havingClause: Option<HavingClause> = Option.none(),
    // windowClause: Option<WindowClause[]> = Option.none(),
    // sortClause: Option<SortBy[]> = Option.none(),
    public withClause: Option<WithClause> = Option.none(),
    // whereClause: Option<WhereClause> = Option.none(),
    // distinctClause: Option<DistinctClause[]> = Option.none(),
    // intoClause: Option<IntoClause> = Option.none(),
    // valuesLists: Option<Object[]> = Option.none()) {
  ) {
    super();
  }
}
