import { Either, Try } from 'funfix-core';

export class Alias {
  /**
   *
   * @param js { Alias: { aliasname: string } }
   */
  public static fromJs(js: any): Either<Error, Alias> {
    return Try.of(() => js && js.Alias && js.Alias.aliasname)
      .fold(
      () => Either.left(new Error('Missing Alias.aliasname field')),
      (name) => Either.right(new Alias(name)))
  }

  constructor(public name: string) { }
}
