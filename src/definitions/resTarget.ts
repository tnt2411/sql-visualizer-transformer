import { Either, Try, Option } from 'funfix-core';
import { ColumnRef } from './columnRef';
import { AConst } from './aConst';
import { TypeCast } from './typeCast';
import { FuncCall } from './funcCall';

export type ResTargetVal = ColumnRef | TypeCast | FuncCall | AConst; // ColumnRef | AConst | ParamRef

export class ResTarget {
  public static fromJs(js: any): Either<Error, ResTarget> {
    return Try.of(() => js && js.ResTarget)
      .fold(
      () => Either.left<Error, any>(new Error('Expected nonempty ResTarget field')),
      (target) => Either.right<Error, any>(target))
      .flatMap((target) => {
        const parsers: Array<(_: any) => Either<Error, ResTargetVal>> = [ColumnRef.fromJs, TypeCast.fromJs, FuncCall.fromJs, AConst.fromJs];
        const reduced = parsers.reduce(
          (prev, cur) => prev.isRight() ? prev : cur(target.val),
          Either.left<Error, ResTargetVal>(new Error()))
        return (reduced.isRight() ? reduced : Either.left<Error, ResTargetVal>(new Error(`No matching parser for ResTargetVal ${JSON.stringify(js)}`)))
          .map((val) => new ResTarget(val, Option.of(js.ResTarget.name)));
      })
  }

  constructor(public value: ResTargetVal, public name: Option<string> = Option.none()) { }
}
