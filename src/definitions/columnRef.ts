import { Either, Try } from 'funfix-core';
import { StringVal } from './stringVal';
import { isArray } from 'util';
import { AStar } from './aStar';

export type ColumnRefField = StringVal | AStar;

export class ColumnRef {
  public static fromJs(js: any): Either<Error, ColumnRef> {
    return Try.of(() => js && js.ColumnRef && js.ColumnRef.fields)
      .fold(() => Either.left(new Error('Missing ColumnRef.fields field')), (fields) => Either.right(fields))
      .flatMap((fields) => isArray(fields) ? Either.right<Error, any[]>(fields) : Either.left<Error, any[]>(new Error('ColumnRef.fields is not an array')))
      .flatMap((fields) => {
        const parsers: Array<(_) => Either<Error, ColumnRefField>> = [StringVal.fromJs, AStar.fromJs];
        const fs = fields.map((field) => {
          const reduced = parsers.reduce(
            (prev, cur) => prev.isRight() ? prev : cur(field),
            Either.left<Error, ColumnRefField>(new Error()));
          return (reduced.isRight() ? reduced : Either.left<Error, ColumnRefField>(new Error(`No matching parser for ColumnRefField ${JSON.stringify(js)}`)))
        });
        const res: Either<Error, ColumnRefField[]> = fs.every((_) => _.isRight()) ?
          Either.right(fs.map((_) => _.get())) :
          Either.left(new Error('Not all fields are valid'));
        return res;
      })
      .map((fields) => new ColumnRef(fields));
  }

  constructor(public fields: ColumnRefField[]) { }
}
