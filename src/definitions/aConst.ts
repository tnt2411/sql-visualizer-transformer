import { Either, Try } from 'funfix-core';
import { IntegerVal } from './integerVal';

export class AConst {
  /**
   *
   * @param js { A_Const: { val: IntegerVal; location: number; } }
  }
   */
  public static fromJs(js: any): Either<Error, AConst> {
    return Try.of(() => js && js.A_Const.val)
      .fold(() => Either.left(new Error('Expected nonempty val field')), (_) => Either.right(_))
      .flatMap((_) => IntegerVal.fromJs(_))
      .map((_) => new AConst(_));
  }

  constructor(public value: IntegerVal) { }
}
