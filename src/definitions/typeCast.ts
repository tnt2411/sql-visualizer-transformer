import { Either, Try } from 'funfix-core';
import { ColumnRef } from './columnRef';
import { TypeName } from './typeName';
import { nonEmpty } from '../util';

export class TypeCast {
  /**
   *
   * @param js { TypeCast: { arg: ColumnRef; typeName: TypeName; }}
   */
  public static fromJs(js: any): Either<Error, TypeCast> {
    return nonEmpty(() => js && js.TypeCast && [js.TypeCast.arg, js.TypeCast.typeName])
      .flatMap(([arg, typeName]) => {
        const argE = ColumnRef.fromJs(arg);
        const typeNamE = TypeName.fromJs(typeName);
        return argE.flatMap((a) => typeNamE.map((t) => new TypeCast(a, t)))
      })
  }

  constructor(public arg: ColumnRef, typeName: TypeName) { }
}
