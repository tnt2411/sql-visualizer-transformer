import { SelectStmt } from './selectStmt';
import { Either, Option } from 'funfix-core';
import { Statement } from './statement';

export class Query {
  public static fromJs(js: any): Either<Error, Query> {
    let res: Either<Error, Query>;

    if (!js) {
      res = Either.left(new Error('Expected nonempty value for Query'));
    } else {
      const parsers: Array<(_: any) => Either<Error, Query>> = [
        (_: any) => SelectStmt.fromJs(_.SelectStmt).map((s) => new Query(Option.some(s)))
      ];
      res = parsers.reduce((prev, cur) => prev.isRight() ? prev : cur(js), Either.left<Error, Query>(new Error('No matching parser for Query')));
    }
    return res;
  }

  constructor(public SelectStmt: Option<SelectStmt> = Option.none()) { }
}
