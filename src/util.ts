import * as lodash from 'lodash';
import { Either, Try, Option } from 'funfix-core';
import { Map } from 'immutable';

export function nonEmpty<T>(v: () => T, message = ''): Either<Error, T> {
  return Try.of(() => Option.of(v()))
    .fold(
    () => Either.left(new Error('Unsafe evaluation')),
    (opt) => opt.map((_) => Either.right<Error, T>(_)).getOrElse(Either.left(new Error(`Expected nonempty value ${message}`))));
}

export function toMap<K, V>(values: V[], key: (_: V) => K): Map<K, V> {
  return Map(values.map((v) => [key(v), v]));
}

export function flattenEither<L, R>(eithers: Array<Either<L, R>>): Either<L, R[]> {
  return eithers.reduce((prev, cur) => prev.flatMap((list) => cur.map((_) => list.concat(_))), Either.right([]));
}

export function optionToArray<T>(option: Option<T>): T[] {
  return option.map((_) => [_]).getOrElse([]);
}

export function flattenOptions<T>(options: Array<Option<T>>): T[] {
  return lodash.flatten(options.map((_) => optionToArray(_)));
}
