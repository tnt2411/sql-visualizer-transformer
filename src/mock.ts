import * as lodash from 'lodash';
import { Tree, Node } from './tree';
import { Query } from './definitions/query';

export function tree(): Tree {
  return new Tree(new Node({
    title: 'result',
    label: 'potato'
  }, lodash.times(2, () => new Node({
    title: lodash.uniqueId(),
    label: lodash.uniqueId()
  }, lodash.times(2, () => new Node({
    title: lodash.uniqueId(),
    label: lodash.uniqueId()
  }))))));
}

export function simpleSelectStatement() {
  return `SELECT * FROM y;`;
}

export function sqlQuery(): string {
  return `
    with daily_users as (
      with daily_girl_users as (
        select
          logged_at::time as time_d,
          count(1) as num_girl_users
        from potato
      )
      select
        created_at::date as date_d,
        count(1) as num_users
      from users, daily_girl_users
      group by 1
    ), daily_products as (
      select
        created_at::date as date_d,
        count(1) as num_products
      from products, comments
      group by 1
    ), daily_reviews as (
      select
        created_at::date as date_d,
        count(1) as num_reviews
      from reviews
      group by 1
    )
    select
      U.date_d,
      U.new_users,
      P.new_products,
      R.new_reviews
    from daily_users U, daily_products P, daily_reviews R
    where U.date_d = P.date_d
    and U.date_d = R.date_d
    order by 1 DESC
  `;
}
