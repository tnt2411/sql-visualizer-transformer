import { MermaidTransformer } from './mermaid';
import { expect } from 'chai';
import * as mermaid from 'mermaid';
import * as mock from './mock';
import 'mocha';

describe('MermaidTransformer', () => {
  const transformer = new MermaidTransformer();

  it('should transform a SQL SELECT statement into a mermaid graph', () => {
    const res = transformer.transform(mock.simpleSelectStatement());
    expect(res.isRight()).to.equal(true);
    const graph = res.get()[0];
    expect(() => mermaid.parse(graph)).not.to.throw();
  });

  it('should transform a SQL query into a mermaid graph', () => {
    const res = transformer.transform(mock.sqlQuery());
    expect(res.isRight()).to.equal(true);
    const graph = res.get()[0];
    expect(() => mermaid.parse(graph)).not.to.throw();
  });
});
