import * as lodash from 'lodash';
import { Tree, Node, TreeTransformer } from './tree';
import { Either } from 'funfix-core';
import { Refiner } from './refiner';
import { flattenEither, optionToArray } from './util';

export class MermaidTransformer {

  /**
   * The main transformation function
   * From a PostgreSQL query to its corresponding Mermaid graphs
   * @param query PostgreSQL query
   * @returns list of mermaid graphs
   */
  public transform(query: string): Either<Error, string[]> {
    const refiner = new Refiner();
    const treeTransformer = new TreeTransformer();
    return refiner.transform(query)
      .map((queries) => queries.map((_) => treeTransformer.transform(_)))
      .map((trees) => lodash.flatten(trees.map((_) => optionToArray(_))))
      .map((trees) => trees.map((_) => this.fromTree(_)));
  }

  /**
   * Transforming a tree into its corresponding Mermaid graph
   * @param tree
   */
  private fromTree(tree: Tree): string {
    return ['graph LR'].concat(this.nodes(tree).map((_) => this.node(_))).join('\n').trim();
  }

  /**
   * Get all uniques nodes in a given tree
   * @param tree
   */
  private nodes(tree: Tree): Node[] {
    return lodash.uniq(this.nodesRecursively([tree.root], tree.root.children as Node[]));
  }

  private nodesRecursively(total: Node[], remains: Node[]): Node[] {
    if (remains.length === 0) {
      return total;
    } else {
      const [head, ...tail] = remains;
      return this.nodesRecursively(total.concat(head), tail.concat(head.children as Node[]));
    }
  }

  private node(node: Node): string {
    return [this.content(node)].concat(node.children.map((child: Node) => `${this.content(child)}-->${this.content(node)}`))
      .join('\n');
  }

  private content(node: Node): string {
    return `${node.value.title}(${node.value.title}<br/>${node.value.label})`;
  }
}
