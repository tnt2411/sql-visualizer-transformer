import { TreeTransformer } from './tree';
import { sqlQuery } from './mock';
import { Refiner } from './refiner';
import { expect } from 'chai';
import 'mocha';

describe('TreeTransformer', () => {
  const transformer = new TreeTransformer();
  const refiner = new Refiner();

  it('should transform a query to a tree', () => {
    const query = refiner.transform(sqlQuery());
    expect(query.isRight());
    const res = transformer.transform(query.get()[0]);
    expect(res.nonEmpty()).to.equal(true);
  });
});
