import * as queryParser from 'pg-query-parser';
import { Query } from './definitions/query';
import { Either } from 'funfix-core';
import { flattenEither } from './util';

export class Refiner {
  /**
   * Parse a PostgreSQL to its corresponding queries
   * @param sqlQuery
   */
  public transform(sqlQuery: string): Either<Error, Query[]> {
    return this.toJs(sqlQuery).flatMap((queries) =>
      flattenEither(queries.map((query) => this.refine(query))));
  }

  private toJs(query: string): Either<Error, object[]> {
    const parsed = queryParser.parse(query);
    return parsed.error ? Either.left(new Error(parsed.error.message)) : Either.right(parsed.query);
  }

  private refine(value: any): Either<Error, Query> {
    return Query.fromJs(value);
  }
}
