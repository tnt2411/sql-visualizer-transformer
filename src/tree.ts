import * as lodash from 'lodash';
import { Query } from './definitions/query';
import { SelectStmt } from './definitions/selectStmt';
import { ResTarget } from './definitions/resTarget';
import { ColumnRef } from './definitions/columnRef';
import { TypeCast } from './definitions/typeCast';
import { FuncCall } from './definitions/funcCall';
import { Option } from 'funfix-core';
import { CommonTableExpr } from './definitions/commonTableExpr';
import { Map } from 'immutable';
import { FromClauseMember } from './definitions/fromClause';
import { flattenOptions } from './util';
import { AConst } from './definitions/aConst';
import { StringVal } from './definitions/stringVal';
import { AStar } from './definitions/aStar';

export class TreeTransformer {
  /**
   * Gather all nodes
   * Perform topological sort
   * Gather all completed nodes
   * Build the tree
   */
  public transform(query: Query): Option<Tree> {
    return query.SelectStmt
      .map((_) => this.fromSelectStatement(_))
      .map((_) => new Tree(_));
  }

  private fromSelectStatement(statement: SelectStmt): Node {
    return this.fromSelectStatementRecursively(statement, 'result').node;
  }

  /**
   * Transform a SELECT statement to a Node
   * In-order traversal
   * Assuming all CTEs are in topological order
   * Only cache CTE nodes
   * @param statement
   */
  private fromSelectStatementRecursively(statement: SelectStmt, title: string): { node: Node; cache: Map<string, Node>; } {
    // First, transform its CTEs and cache them
    const ctes: CommonTableExpr[] = statement.withClause.map((_) => _.ctes).getOrElse([]);
    const cache = ctes.reduce((prev, cur) => {
      return cur.query.SelectStmt.map((s) => {
        const { node, cache } = this.fromSelectStatementRecursively(s, cur.name);
        return prev.merge(cache);
      }).getOrElse(prev);
    }, Map<string, Node>());

    // Then, gather the children and create the node
    const node = new Node(this.nodeValue(statement, title), this.children(statement, cache));
    return {
      node,
      cache: cache.set(title, node)
    };
  }

  private label(stmt: SelectStmt, original = false): string {
    return lodash.uniq(flattenOptions(stmt.targetList.map((target) => this.fromResTarget(target, original))))
      .join(', ');
  }

  private fromResTarget(target: ResTarget, original = false): Option<string> {
    return (original ? Option.none() : target.name).orElseL(() => {
      let res: Option<string>;
      if (target.value instanceof ColumnRef) {
        res = this.fromColumnRef(target.value);
      } else if (target.value instanceof TypeCast) {
        res = this.fromColumnRef(target.value.arg);
      } else if (target.value instanceof FuncCall) {
        res = original ? Option.none() : Option.of(target.name.getOrElse(target.value.name.map((_) => _.value).join('.')));
      } else if (target.value instanceof AConst) {
        res = Option.of(target.value.value.value.toString());
      } else {
        res = Option.none();
      }
      return res;
    });
  }

  private fromColumnRef(ref: ColumnRef): Option<string> {
    return Option.of(lodash.last(ref.fields)).flatMap((field) => {
      if (field instanceof StringVal) {
        return Option.of(field.value);
      } else if (field instanceof AStar) {
        return Option.of('*');
      } else {
        return Option.none();
      }
    });
  }

  private children(statement: SelectStmt, cache: Map<string, Node>): Node[] {
    const from: FromClauseMember[] = statement.fromClause.map((_) => _.members).getOrElse([]);

    const c: Node[] = flattenOptions(from.map((child) => Option.of(cache.get(child.relname))
      .fold(() => {
        const label = this.label(statement, true);
        return label === '*' ? Option.none() : Option.some(new Node({ title: child.relname, label: this.label(statement, true) }, []));
      }, (_) => Option.some(_))));
    return c;
  }

  private nodeValue(statement: SelectStmt, title: string): NodeValue {
    return {
      title,
      label: this.label(statement)
    };
  }
}

export interface NodeValue {
  title: string;
  label: string;
}

export class Tree {
  constructor(public root: Node) { }
}

export class Node {
  constructor(public value: NodeValue, public children: Array<Node> = []) { }
}
