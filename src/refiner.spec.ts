import { Refiner } from './refiner';
import { sqlQuery } from './mock';
import { expect } from 'chai';
import 'mocha';

describe('Refiner', () => {
  const refiner = new Refiner();

  it('should refine the given query', () => {
    const res = refiner.transform(sqlQuery());
    expect(res.isRight()).to.equal(true);
    const query = res.get()[0];
    expect(query.SelectStmt.nonEmpty()).to.equal(true);
    expect(query.SelectStmt.get().fromClause.nonEmpty()).to.equal(true);
    expect(query.SelectStmt.get().withClause.nonEmpty()).to.equal(true);
    expect(query.SelectStmt.get().targetList.length).to.equal(4);
  });
});
