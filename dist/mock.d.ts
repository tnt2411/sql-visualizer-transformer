import { Tree } from './tree';
export declare function tree(): Tree;
export declare function simpleSelectStatement(): string;
export declare function sqlQuery(): string;
