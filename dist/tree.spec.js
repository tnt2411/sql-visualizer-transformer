(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./tree", "./mock", "./refiner", "chai", "mocha"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var tree_1 = require("./tree");
    var mock_1 = require("./mock");
    var refiner_1 = require("./refiner");
    var chai_1 = require("chai");
    require("mocha");
    describe('TreeTransformer', function () {
        var transformer = new tree_1.TreeTransformer();
        var refiner = new refiner_1.Refiner();
        it('should transform a query to a tree', function () {
            var query = refiner.transform(mock_1.sqlQuery());
            chai_1.expect(query.isRight());
            var res = transformer.transform(query.get()[0]);
            chai_1.expect(res.nonEmpty()).to.equal(true);
        });
    });
});
//# sourceMappingURL=tree.spec.js.map