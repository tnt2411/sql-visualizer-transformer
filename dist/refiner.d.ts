import { Query } from './definitions/query';
import { Either } from 'funfix-core';
export declare class Refiner {
    /**
     * Parse a PostgreSQL to its corresponding queries
     * @param sqlQuery
     */
    transform(sqlQuery: string): Either<Error, Query[]>;
    private toJs(query);
    private refine(value);
}
