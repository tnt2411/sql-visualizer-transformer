(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "./tree", "./refiner", "./util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var lodash = require("lodash");
    var tree_1 = require("./tree");
    var refiner_1 = require("./refiner");
    var util_1 = require("./util");
    var MermaidTransformer = /** @class */ (function () {
        function MermaidTransformer() {
        }
        /**
         * The main transformation function
         * From a PostgreSQL query to its corresponding Mermaid graphs
         * @param query PostgreSQL query
         * @returns list of mermaid graphs
         */
        MermaidTransformer.prototype.transform = function (query) {
            var _this = this;
            var refiner = new refiner_1.Refiner();
            var treeTransformer = new tree_1.TreeTransformer();
            return refiner.transform(query)
                .map(function (queries) { return queries.map(function (_) { return treeTransformer.transform(_); }); })
                .map(function (trees) { return lodash.flatten(trees.map(function (_) { return util_1.optionToArray(_); })); })
                .map(function (trees) { return trees.map(function (_) { return _this.fromTree(_); }); });
        };
        /**
         * Transforming a tree into its corresponding Mermaid graph
         * @param tree
         */
        MermaidTransformer.prototype.fromTree = function (tree) {
            var _this = this;
            return ['graph LR'].concat(this.nodes(tree).map(function (_) { return _this.node(_); })).join('\n').trim();
        };
        /**
         * Get all uniques nodes in a given tree
         * @param tree
         */
        MermaidTransformer.prototype.nodes = function (tree) {
            return lodash.uniq(this.nodesRecursively([tree.root], tree.root.children));
        };
        MermaidTransformer.prototype.nodesRecursively = function (total, remains) {
            if (remains.length === 0) {
                return total;
            }
            else {
                var head = remains[0], tail = remains.slice(1);
                return this.nodesRecursively(total.concat(head), tail.concat(head.children));
            }
        };
        MermaidTransformer.prototype.node = function (node) {
            var _this = this;
            return [this.content(node)].concat(node.children.map(function (child) { return _this.content(child) + "-->" + _this.content(node); }))
                .join('\n');
        };
        MermaidTransformer.prototype.content = function (node) {
            return node.value.title + "(" + node.value.title + "<br/>" + node.value.label + ")";
        };
        return MermaidTransformer;
    }());
    exports.MermaidTransformer = MermaidTransformer;
});
//# sourceMappingURL=mermaid.js.map