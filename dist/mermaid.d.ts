import { Either } from 'funfix-core';
export declare class MermaidTransformer {
    /**
     * The main transformation function
     * From a PostgreSQL query to its corresponding Mermaid graphs
     * @param query PostgreSQL query
     * @returns list of mermaid graphs
     */
    transform(query: string): Either<Error, string[]>;
    /**
     * Transforming a tree into its corresponding Mermaid graph
     * @param tree
     */
    private fromTree(tree);
    /**
     * Get all uniques nodes in a given tree
     * @param tree
     */
    private nodes(tree);
    private nodesRecursively(total, remains);
    private node(node);
    private content(node);
}
