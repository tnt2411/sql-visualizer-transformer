import { Query } from './definitions/query';
import { Option } from 'funfix-core';
export declare class TreeTransformer {
    /**
     * Gather all nodes
     * Perform topological sort
     * Gather all completed nodes
     * Build the tree
     */
    transform(query: Query): Option<Tree>;
    private fromSelectStatement(statement);
    /**
     * Transform a SELECT statement to a Node
     * In-order traversal
     * Assuming all CTEs are in topological order
     * Only cache CTE nodes
     * @param statement
     */
    private fromSelectStatementRecursively(statement, title);
    private label(stmt, original?);
    private fromResTarget(target, original?);
    private fromColumnRef(ref);
    private children(statement, cache);
    private nodeValue(statement, title);
}
export interface NodeValue {
    title: string;
    label: string;
}
export declare class Tree {
    root: Node;
    constructor(root: Node);
}
export declare class Node {
    value: NodeValue;
    children: Array<Node>;
    constructor(value: NodeValue, children?: Array<Node>);
}
