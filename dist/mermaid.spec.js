(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./mermaid", "chai", "mermaid", "./mock", "mocha"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var mermaid_1 = require("./mermaid");
    var chai_1 = require("chai");
    var mermaid = require("mermaid");
    var mock = require("./mock");
    require("mocha");
    describe('MermaidTransformer', function () {
        var transformer = new mermaid_1.MermaidTransformer();
        it('should transform a SQL SELECT statement into a mermaid graph', function () {
            var res = transformer.transform(mock.simpleSelectStatement());
            chai_1.expect(res.isRight()).to.equal(true);
            var graph = res.get()[0];
            chai_1.expect(function () { return mermaid.parse(graph); }).not.to.throw();
        });
        it('should transform a SQL query into a mermaid graph', function () {
            var res = transformer.transform(mock.sqlQuery());
            chai_1.expect(res.isRight()).to.equal(true);
            var graph = res.get()[0];
            chai_1.expect(function () { return mermaid.parse(graph); }).not.to.throw();
        });
    });
});
//# sourceMappingURL=mermaid.spec.js.map