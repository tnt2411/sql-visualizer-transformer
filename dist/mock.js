(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "./tree"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var lodash = require("lodash");
    var tree_1 = require("./tree");
    function tree() {
        return new tree_1.Tree(new tree_1.Node({
            title: 'result',
            label: 'potato'
        }, lodash.times(2, function () { return new tree_1.Node({
            title: lodash.uniqueId(),
            label: lodash.uniqueId()
        }, lodash.times(2, function () { return new tree_1.Node({
            title: lodash.uniqueId(),
            label: lodash.uniqueId()
        }); })); })));
    }
    exports.tree = tree;
    function simpleSelectStatement() {
        return "SELECT * FROM y;";
    }
    exports.simpleSelectStatement = simpleSelectStatement;
    function sqlQuery() {
        return "\n    with daily_users as (\n      with daily_girl_users as (\n        select\n          logged_at::time as time_d,\n          count(1) as num_girl_users\n        from potato\n      )\n      select\n        created_at::date as date_d,\n        count(1) as num_users\n      from users, daily_girl_users\n      group by 1\n    ), daily_products as (\n      select\n        created_at::date as date_d,\n        count(1) as num_products\n      from products, comments\n      group by 1\n    ), daily_reviews as (\n      select\n        created_at::date as date_d,\n        count(1) as num_reviews\n      from reviews\n      group by 1\n    )\n    select\n      U.date_d,\n      U.new_users,\n      P.new_products,\n      R.new_reviews\n    from daily_users U, daily_products P, daily_reviews R\n    where U.date_d = P.date_d\n    and U.date_d = R.date_d\n    order by 1 DESC\n  ";
    }
    exports.sqlQuery = sqlQuery;
});
//# sourceMappingURL=mock.js.map