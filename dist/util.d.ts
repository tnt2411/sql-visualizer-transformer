import { Either, Option } from 'funfix-core';
import { Map } from 'immutable';
export declare function nonEmpty<T>(v: () => T, message?: string): Either<Error, T>;
export declare function toMap<K, V>(values: V[], key: (_: V) => K): Map<K, V>;
export declare function flattenEither<L, R>(eithers: Array<Either<L, R>>): Either<L, R[]>;
export declare function optionToArray<T>(option: Option<T>): T[];
export declare function flattenOptions<T>(options: Array<Option<T>>): T[];
