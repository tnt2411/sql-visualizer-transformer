(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./refiner", "./mock", "chai", "mocha"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var refiner_1 = require("./refiner");
    var mock_1 = require("./mock");
    var chai_1 = require("chai");
    require("mocha");
    describe('Refiner', function () {
        var refiner = new refiner_1.Refiner();
        it('should refine the given query', function () {
            var res = refiner.transform(mock_1.sqlQuery());
            chai_1.expect(res.isRight()).to.equal(true);
            var query = res.get()[0];
            chai_1.expect(query.SelectStmt.nonEmpty()).to.equal(true);
            chai_1.expect(query.SelectStmt.get().fromClause.nonEmpty()).to.equal(true);
            chai_1.expect(query.SelectStmt.get().withClause.nonEmpty()).to.equal(true);
            chai_1.expect(query.SelectStmt.get().targetList.length).to.equal(4);
        });
    });
});
//# sourceMappingURL=refiner.spec.js.map