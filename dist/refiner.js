(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "pg-query-parser", "./definitions/query", "funfix-core", "./util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var queryParser = require("pg-query-parser");
    var query_1 = require("./definitions/query");
    var funfix_core_1 = require("funfix-core");
    var util_1 = require("./util");
    var Refiner = /** @class */ (function () {
        function Refiner() {
        }
        /**
         * Parse a PostgreSQL to its corresponding queries
         * @param sqlQuery
         */
        Refiner.prototype.transform = function (sqlQuery) {
            var _this = this;
            return this.toJs(sqlQuery).flatMap(function (queries) {
                return util_1.flattenEither(queries.map(function (query) { return _this.refine(query); }));
            });
        };
        Refiner.prototype.toJs = function (query) {
            var parsed = queryParser.parse(query);
            return parsed.error ? funfix_core_1.Either.left(new Error(parsed.error.message)) : funfix_core_1.Either.right(parsed.query);
        };
        Refiner.prototype.refine = function (value) {
            return query_1.Query.fromJs(value);
        };
        return Refiner;
    }());
    exports.Refiner = Refiner;
});
//# sourceMappingURL=refiner.js.map