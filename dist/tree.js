(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "./definitions/columnRef", "./definitions/typeCast", "./definitions/funcCall", "funfix-core", "immutable", "./util", "./definitions/aConst", "./definitions/stringVal", "./definitions/aStar"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var lodash = require("lodash");
    var columnRef_1 = require("./definitions/columnRef");
    var typeCast_1 = require("./definitions/typeCast");
    var funcCall_1 = require("./definitions/funcCall");
    var funfix_core_1 = require("funfix-core");
    var immutable_1 = require("immutable");
    var util_1 = require("./util");
    var aConst_1 = require("./definitions/aConst");
    var stringVal_1 = require("./definitions/stringVal");
    var aStar_1 = require("./definitions/aStar");
    var TreeTransformer = /** @class */ (function () {
        function TreeTransformer() {
        }
        /**
         * Gather all nodes
         * Perform topological sort
         * Gather all completed nodes
         * Build the tree
         */
        TreeTransformer.prototype.transform = function (query) {
            var _this = this;
            return query.SelectStmt
                .map(function (_) { return _this.fromSelectStatement(_); })
                .map(function (_) { return new Tree(_); });
        };
        TreeTransformer.prototype.fromSelectStatement = function (statement) {
            return this.fromSelectStatementRecursively(statement, 'result').node;
        };
        /**
         * Transform a SELECT statement to a Node
         * In-order traversal
         * Assuming all CTEs are in topological order
         * Only cache CTE nodes
         * @param statement
         */
        TreeTransformer.prototype.fromSelectStatementRecursively = function (statement, title) {
            var _this = this;
            // First, transform its CTEs and cache them
            var ctes = statement.withClause.map(function (_) { return _.ctes; }).getOrElse([]);
            var cache = ctes.reduce(function (prev, cur) {
                return cur.query.SelectStmt.map(function (s) {
                    var _a = _this.fromSelectStatementRecursively(s, cur.name), node = _a.node, cache = _a.cache;
                    return prev.merge(cache);
                }).getOrElse(prev);
            }, immutable_1.Map());
            // Then, gather the children and create the node
            var node = new Node(this.nodeValue(statement, title), this.children(statement, cache));
            return {
                node: node,
                cache: cache.set(title, node)
            };
        };
        TreeTransformer.prototype.label = function (stmt, original) {
            var _this = this;
            if (original === void 0) { original = false; }
            return lodash.uniq(util_1.flattenOptions(stmt.targetList.map(function (target) { return _this.fromResTarget(target, original); })))
                .join(', ');
        };
        TreeTransformer.prototype.fromResTarget = function (target, original) {
            var _this = this;
            if (original === void 0) { original = false; }
            return (original ? funfix_core_1.Option.none() : target.name).orElseL(function () {
                var res;
                if (target.value instanceof columnRef_1.ColumnRef) {
                    res = _this.fromColumnRef(target.value);
                }
                else if (target.value instanceof typeCast_1.TypeCast) {
                    res = _this.fromColumnRef(target.value.arg);
                }
                else if (target.value instanceof funcCall_1.FuncCall) {
                    res = original ? funfix_core_1.Option.none() : funfix_core_1.Option.of(target.name.getOrElse(target.value.name.map(function (_) { return _.value; }).join('.')));
                }
                else if (target.value instanceof aConst_1.AConst) {
                    res = funfix_core_1.Option.of(target.value.value.value.toString());
                }
                else {
                    res = funfix_core_1.Option.none();
                }
                return res;
            });
        };
        TreeTransformer.prototype.fromColumnRef = function (ref) {
            return funfix_core_1.Option.of(lodash.last(ref.fields)).flatMap(function (field) {
                if (field instanceof stringVal_1.StringVal) {
                    return funfix_core_1.Option.of(field.value);
                }
                else if (field instanceof aStar_1.AStar) {
                    return funfix_core_1.Option.of('*');
                }
                else {
                    return funfix_core_1.Option.none();
                }
            });
        };
        TreeTransformer.prototype.children = function (statement, cache) {
            var _this = this;
            var from = statement.fromClause.map(function (_) { return _.members; }).getOrElse([]);
            var c = util_1.flattenOptions(from.map(function (child) { return funfix_core_1.Option.of(cache.get(child.relname))
                .fold(function () {
                var label = _this.label(statement, true);
                return label === '*' ? funfix_core_1.Option.none() : funfix_core_1.Option.some(new Node({ title: child.relname, label: _this.label(statement, true) }, []));
            }, function (_) { return funfix_core_1.Option.some(_); }); }));
            return c;
        };
        TreeTransformer.prototype.nodeValue = function (statement, title) {
            return {
                title: title,
                label: this.label(statement)
            };
        };
        return TreeTransformer;
    }());
    exports.TreeTransformer = TreeTransformer;
    var Tree = /** @class */ (function () {
        function Tree(root) {
            this.root = root;
        }
        return Tree;
    }());
    exports.Tree = Tree;
    var Node = /** @class */ (function () {
        function Node(value, children) {
            if (children === void 0) { children = []; }
            this.value = value;
            this.children = children;
        }
        return Node;
    }());
    exports.Node = Node;
});
//# sourceMappingURL=tree.js.map