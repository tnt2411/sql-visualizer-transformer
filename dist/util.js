(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "lodash", "funfix-core", "immutable"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var lodash = require("lodash");
    var funfix_core_1 = require("funfix-core");
    var immutable_1 = require("immutable");
    function nonEmpty(v, message) {
        if (message === void 0) { message = ''; }
        return funfix_core_1.Try.of(function () { return funfix_core_1.Option.of(v()); })
            .fold(function () { return funfix_core_1.Either.left(new Error('Unsafe evaluation')); }, function (opt) { return opt.map(function (_) { return funfix_core_1.Either.right(_); }).getOrElse(funfix_core_1.Either.left(new Error("Expected nonempty value " + message))); });
    }
    exports.nonEmpty = nonEmpty;
    function toMap(values, key) {
        return immutable_1.Map(values.map(function (v) { return [key(v), v]; }));
    }
    exports.toMap = toMap;
    function flattenEither(eithers) {
        return eithers.reduce(function (prev, cur) { return prev.flatMap(function (list) { return cur.map(function (_) { return list.concat(_); }); }); }, funfix_core_1.Either.right([]));
    }
    exports.flattenEither = flattenEither;
    function optionToArray(option) {
        return option.map(function (_) { return [_]; }).getOrElse([]);
    }
    exports.optionToArray = optionToArray;
    function flattenOptions(options) {
        return lodash.flatten(options.map(function (_) { return optionToArray(_); }));
    }
    exports.flattenOptions = flattenOptions;
});
//# sourceMappingURL=util.js.map