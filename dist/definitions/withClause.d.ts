import { Either } from 'funfix-core';
import { CommonTableExpr } from './commonTableExpr';
export declare class WithClause {
    ctes: CommonTableExpr[];
    static fromJs(js: any): Either<Error, WithClause>;
    constructor(ctes: CommonTableExpr[]);
}
