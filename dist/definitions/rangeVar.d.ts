import { Option, Either } from 'funfix-core';
import { Alias } from './alias';
/**
  export interface RangeVar {
  RangeVar: {
    relname: string;
    inhOpt: number;
    relpersistence: string;
    location: number;
    alias?: {
      Alias: {
        aliasname: string;
      };
    };
  };
}
 */
export declare class RangeVar {
    relname: string;
    alias: Option<Alias>;
    static fromJs(js: any): Either<Error, RangeVar>;
    constructor(relname: string, alias?: Option<Alias>);
}
