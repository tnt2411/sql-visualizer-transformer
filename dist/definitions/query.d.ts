import { SelectStmt } from './selectStmt';
import { Either, Option } from 'funfix-core';
export declare class Query {
    SelectStmt: Option<SelectStmt>;
    static fromJs(js: any): Either<Error, Query>;
    constructor(SelectStmt?: Option<SelectStmt>);
}
