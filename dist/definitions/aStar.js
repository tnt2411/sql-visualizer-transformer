(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var util_1 = require("../util");
    var AStar = /** @class */ (function () {
        function AStar() {
        }
        /**
         *
         * @param js {"A_Star":{}}
         */
        AStar.fromJs = function (js) {
            return util_1.nonEmpty(function () { return js && js.A_Star; }, 'A_Star')
                .map(function () { return new AStar(); });
        };
        return AStar;
    }());
    exports.AStar = AStar;
});
//# sourceMappingURL=aStar.js.map