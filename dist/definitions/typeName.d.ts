import { StringVal } from './stringVal';
import { Either } from 'funfix-core';
export declare class TypeName {
    names: StringVal[];
    /**
     *
     * @param js {
                                  'TypeName': {
                                    'names': [
                                      {
                                        'String': {
                                          'str': 'date'
                                        }
                                      }
                                    ],
                                    'typemod': -1,
                                    'location': 47
                                  }
                                }
     */
    static fromJs(js: any): Either<Error, TypeName>;
    constructor(names: StringVal[]);
}
