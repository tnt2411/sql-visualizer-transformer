(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./selectStmt", "funfix-core"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var selectStmt_1 = require("./selectStmt");
    var funfix_core_1 = require("funfix-core");
    var Query = /** @class */ (function () {
        function Query(SelectStmt) {
            if (SelectStmt === void 0) { SelectStmt = funfix_core_1.Option.none(); }
            this.SelectStmt = SelectStmt;
        }
        Query.fromJs = function (js) {
            var res;
            if (!js) {
                res = funfix_core_1.Either.left(new Error('Expected nonempty value for Query'));
            }
            else {
                var parsers = [
                    function (_) { return selectStmt_1.SelectStmt.fromJs(_.SelectStmt).map(function (s) { return new Query(funfix_core_1.Option.some(s)); }); }
                ];
                res = parsers.reduce(function (prev, cur) { return prev.isRight() ? prev : cur(js); }, funfix_core_1.Either.left(new Error('No matching parser for Query')));
            }
            return res;
        };
        return Query;
    }());
    exports.Query = Query;
});
//# sourceMappingURL=query.js.map