(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./query"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var query_1 = require("./query");
    var CommonTableExpr = /** @class */ (function () {
        function CommonTableExpr(name, query) {
            this.name = name;
            this.query = query;
        }
        CommonTableExpr.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.CommonTableExpr; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Missing CommonTableExpr field')); }, function (cte) { return funfix_core_1.Either.right(cte); })
                .flatMap(function (cte) {
                var nameE = cte.ctename ? funfix_core_1.Either.right(cte.ctename) : funfix_core_1.Either.left(new Error('Missing CommonTableExpr.ctename field'));
                var queryE = query_1.Query.fromJs(js.CommonTableExpr.ctequery);
                return nameE.flatMap(function (name) { return queryE.map(function (query) { return new CommonTableExpr(name, query); }); });
            });
        };
        return CommonTableExpr;
    }());
    exports.CommonTableExpr = CommonTableExpr;
});
//# sourceMappingURL=commonTableExpr.js.map