(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./alias"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var alias_1 = require("./alias");
    /**
      export interface RangeVar {
      RangeVar: {
        relname: string;
        inhOpt: number;
        relpersistence: string;
        location: number;
        alias?: {
          Alias: {
            aliasname: string;
          };
        };
      };
    }
     */
    var RangeVar = /** @class */ (function () {
        function RangeVar(relname, alias) {
            if (alias === void 0) { alias = funfix_core_1.Option.none(); }
            this.relname = relname;
            this.alias = alias;
        }
        RangeVar.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.RangeVar; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty RangeVar field')); }, function (range) { return funfix_core_1.Either.right(range); })
                .flatMap(function (range) {
                var nameE = range.relname ? funfix_core_1.Either.right(range.relname) : funfix_core_1.Either.left(new Error('Expected nonempty RangeVar.relname'));
                return nameE.map(function (name) { return new RangeVar(name, alias_1.Alias.fromJs(range.alias).toOption()); });
            });
        };
        return RangeVar;
    }());
    exports.RangeVar = RangeVar;
});
//# sourceMappingURL=rangeVar.js.map