import { Either } from 'funfix-core';
export declare class StringVal {
    value: string;
    /**
     *
     * @param js { String: { str: 'u' } }
     */
    static fromJs(js: any): Either<Error, StringVal>;
    constructor(value: string);
}
