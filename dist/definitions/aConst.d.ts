import { Either } from 'funfix-core';
import { IntegerVal } from './integerVal';
export declare class AConst {
    value: IntegerVal;
    /**
     *
     * @param js { A_Const: { val: IntegerVal; location: number; } }
    }
     */
    static fromJs(js: any): Either<Error, AConst>;
    constructor(value: IntegerVal);
}
