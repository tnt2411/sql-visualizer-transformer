import { Either, Option } from 'funfix-core';
import { ColumnRef } from './columnRef';
import { AConst } from './aConst';
import { TypeCast } from './typeCast';
import { FuncCall } from './funcCall';
export declare type ResTargetVal = ColumnRef | TypeCast | FuncCall | AConst;
export declare class ResTarget {
    value: ResTargetVal;
    name: Option<string>;
    static fromJs(js: any): Either<Error, ResTarget>;
    constructor(value: ResTargetVal, name?: Option<string>);
}
