(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./commonTableExpr", "util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var commonTableExpr_1 = require("./commonTableExpr");
    var util_1 = require("util");
    var WithClause = /** @class */ (function () {
        function WithClause(ctes) {
            this.ctes = ctes;
        }
        WithClause.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.WithClause && js.WithClause.ctes; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expeted nonempty WithClause.ctes')); }, function (ctes) { return funfix_core_1.Either.right(ctes); })
                .flatMap(function (ctes) { return util_1.isArray(ctes) ? funfix_core_1.Either.right(ctes) : funfix_core_1.Either.left(new Error('Expected WithClause.ctes to be an array')); })
                .flatMap(function (ctes) { return ctes.map(function (cte) { return commonTableExpr_1.CommonTableExpr.fromJs(cte); })
                .reduce(function (prev, cur) { return prev.flatMap(function (cs) { return cur.map(function (cte) { return cs.concat(cte); }); }); }, funfix_core_1.Either.right([])); })
                .map(function (ctes) { return new WithClause(ctes); });
        };
        return WithClause;
    }());
    exports.WithClause = WithClause;
});
//# sourceMappingURL=withClause.js.map