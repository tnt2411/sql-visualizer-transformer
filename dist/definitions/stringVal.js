(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var util_1 = require("../util");
    var StringVal = /** @class */ (function () {
        function StringVal(value) {
            this.value = value;
        }
        /**
         *
         * @param js { String: { str: 'u' } }
         */
        StringVal.fromJs = function (js) {
            return util_1.nonEmpty(function () { return js && js.String && js.String.str; })
                .map(function (s) { return new StringVal(s); });
        };
        return StringVal;
    }());
    exports.StringVal = StringVal;
});
//# sourceMappingURL=stringVal.js.map