(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./stringVal", "./aConst", "funfix-core", "util", "../util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var stringVal_1 = require("./stringVal");
    var aConst_1 = require("./aConst");
    var funfix_core_1 = require("funfix-core");
    var util_1 = require("util");
    var util_2 = require("../util");
    var FuncCall = /** @class */ (function () {
        function FuncCall(name, args) {
            this.name = name;
            this.args = args;
        }
        /**
         *
         * @param js {
                                  'FuncCall': {
                                    'funcname': StringVal[],
                                    'args': AConst[]
                                  }
                                }
         */
        FuncCall.fromJs = function (js) {
            return util_2.nonEmpty(function () { return js && js.FuncCall && [js.FuncCall.funcname, js.FuncCall.args]; })
                .flatMap(function (_a) {
                var funcname = _a[0], args = _a[1];
                var nameE = (util_1.isArray(funcname) ? funfix_core_1.Either.right(funcname) : funfix_core_1.Either.left(new Error('Expected FuncCall.funcname to be an array')))
                    .flatMap(function (name) { return name.map(function (n) { return stringVal_1.StringVal.fromJs(n); })
                    .reduce(function (prev, cur) { return prev.flatMap(function (list) { return cur.map(function (c) { return list.concat(c); }); }); }, funfix_core_1.Either.right([])); });
                var argsE = (util_1.isArray(args) ? funfix_core_1.Either.right(args) : funfix_core_1.Either.left(new Error('Expected FuncCall.args to be an array')))
                    .flatMap(function (_) { return _.map(function (arg) { return aConst_1.AConst.fromJs(arg); })
                    .reduce(function (prev, cur) { return prev.flatMap(function (list) { return cur.map(function (c) { return list.concat(c); }); }); }, funfix_core_1.Either.right([])); });
                return nameE.flatMap(function (name) { return argsE.map(function (args) { return new FuncCall(name, args); }); });
            });
        };
        return FuncCall;
    }());
    exports.FuncCall = FuncCall;
});
//# sourceMappingURL=funcCall.js.map