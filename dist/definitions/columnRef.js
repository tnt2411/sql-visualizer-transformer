(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./stringVal", "util", "./aStar"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var stringVal_1 = require("./stringVal");
    var util_1 = require("util");
    var aStar_1 = require("./aStar");
    var ColumnRef = /** @class */ (function () {
        function ColumnRef(fields) {
            this.fields = fields;
        }
        ColumnRef.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.ColumnRef && js.ColumnRef.fields; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Missing ColumnRef.fields field')); }, function (fields) { return funfix_core_1.Either.right(fields); })
                .flatMap(function (fields) { return util_1.isArray(fields) ? funfix_core_1.Either.right(fields) : funfix_core_1.Either.left(new Error('ColumnRef.fields is not an array')); })
                .flatMap(function (fields) {
                var parsers = [stringVal_1.StringVal.fromJs, aStar_1.AStar.fromJs];
                var fs = fields.map(function (field) {
                    var reduced = parsers.reduce(function (prev, cur) { return prev.isRight() ? prev : cur(field); }, funfix_core_1.Either.left(new Error()));
                    return (reduced.isRight() ? reduced : funfix_core_1.Either.left(new Error("No matching parser for ColumnRefField " + JSON.stringify(js))));
                });
                var res = fs.every(function (_) { return _.isRight(); }) ?
                    funfix_core_1.Either.right(fs.map(function (_) { return _.get(); })) :
                    funfix_core_1.Either.left(new Error('Not all fields are valid'));
                return res;
            })
                .map(function (fields) { return new ColumnRef(fields); });
        };
        return ColumnRef;
    }());
    exports.ColumnRef = ColumnRef;
});
//# sourceMappingURL=columnRef.js.map