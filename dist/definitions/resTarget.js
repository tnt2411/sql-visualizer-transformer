(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./columnRef", "./aConst", "./typeCast", "./funcCall"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var columnRef_1 = require("./columnRef");
    var aConst_1 = require("./aConst");
    var typeCast_1 = require("./typeCast");
    var funcCall_1 = require("./funcCall");
    var ResTarget = /** @class */ (function () {
        function ResTarget(value, name) {
            if (name === void 0) { name = funfix_core_1.Option.none(); }
            this.value = value;
            this.name = name;
        }
        ResTarget.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.ResTarget; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty ResTarget field')); }, function (target) { return funfix_core_1.Either.right(target); })
                .flatMap(function (target) {
                var parsers = [columnRef_1.ColumnRef.fromJs, typeCast_1.TypeCast.fromJs, funcCall_1.FuncCall.fromJs, aConst_1.AConst.fromJs];
                var reduced = parsers.reduce(function (prev, cur) { return prev.isRight() ? prev : cur(target.val); }, funfix_core_1.Either.left(new Error()));
                return (reduced.isRight() ? reduced : funfix_core_1.Either.left(new Error("No matching parser for ResTargetVal " + JSON.stringify(js))))
                    .map(function (val) { return new ResTarget(val, funfix_core_1.Option.of(js.ResTarget.name)); });
            });
        };
        return ResTarget;
    }());
    exports.ResTarget = ResTarget;
});
//# sourceMappingURL=resTarget.js.map