import { Either } from 'funfix-core';
export declare class IntegerVal {
    value: number;
    /**
     *
     * @param js {
                                        'Integer': {
                                          'ival': number;
                                        }
                                      }
     */
    static fromJs(js: any): Either<Error, IntegerVal>;
    constructor(value: number);
}
