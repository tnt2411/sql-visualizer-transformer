(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./stringVal", "funfix-core", "util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var stringVal_1 = require("./stringVal");
    var funfix_core_1 = require("funfix-core");
    var util_1 = require("util");
    var TypeName = /** @class */ (function () {
        function TypeName(names) {
            this.names = names;
        }
        /**
         *
         * @param js {
                                      'TypeName': {
                                        'names': [
                                          {
                                            'String': {
                                              'str': 'date'
                                            }
                                          }
                                        ],
                                        'typemod': -1,
                                        'location': 47
                                      }
                                    }
         */
        TypeName.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.TypeName && js.TypeName.names; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty TypeName.names field')); }, function (names) { return funfix_core_1.Either.right(names); })
                .flatMap(function (names) { return util_1.isArray(names) ? funfix_core_1.Either.right(names) : funfix_core_1.Either.left(new Error('Expected TypeName.names to be an array')); })
                .flatMap(function (names) { return names.map(function (name) { return stringVal_1.StringVal.fromJs(name); })
                .reduce(function (prev, cur) { return prev.flatMap(function (list) { return cur.map(function (c) { return list.concat(c); }); }); }, funfix_core_1.Either.right([])); })
                .map(function (names) { return new TypeName(names); });
        };
        return TypeName;
    }());
    exports.TypeName = TypeName;
});
//# sourceMappingURL=typeName.js.map