import { Either } from 'funfix-core';
import { Query } from './query';
export declare class CommonTableExpr {
    name: string;
    query: Query;
    static fromJs(js: any): Either<Error, CommonTableExpr>;
    constructor(name: string, query: Query);
}
