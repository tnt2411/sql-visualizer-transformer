import { Either } from 'funfix-core';
import { RangeVar } from './rangeVar';
export declare type FromClauseMember = RangeVar;
export declare class FromClause {
    members: FromClauseMember[];
    /**
     *
     * @param value [FromClauseMember...]
     */
    static fromJs(value: any): Either<Error, FromClause>;
    constructor(members: FromClauseMember[]);
}
