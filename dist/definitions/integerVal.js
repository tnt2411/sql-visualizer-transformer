(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var IntegerVal = /** @class */ (function () {
        function IntegerVal(value) {
            this.value = value;
        }
        /**
         *
         * @param js {
                                            'Integer': {
                                              'ival': number;
                                            }
                                          }
         */
        IntegerVal.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.Integer && js.Integer.ival; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty Integer.ival field')); }, function (_) { return funfix_core_1.Either.right(_); })
                .map(function (_) { return new IntegerVal(_); });
        };
        return IntegerVal;
    }());
    exports.IntegerVal = IntegerVal;
});
//# sourceMappingURL=integerVal.js.map