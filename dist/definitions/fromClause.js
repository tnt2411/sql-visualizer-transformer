(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./rangeVar", "util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var rangeVar_1 = require("./rangeVar");
    var util_1 = require("util");
    var FromClause = /** @class */ (function () {
        function FromClause(members) {
            this.members = members;
        }
        /**
         *
         * @param value [FromClauseMember...]
         */
        FromClause.fromJs = function (value) {
            return funfix_core_1.Option.of(value)
                .fold(function () { return funfix_core_1.Either.left(new Error('Got empty value for FromClause')); }, function (v) { return funfix_core_1.Either.right(v); })
                .flatMap(function (v) { return util_1.isArray(v) ? funfix_core_1.Either.right(v) : funfix_core_1.Either.left(new Error('Expected FromClause to be an array')); })
                .flatMap(function (list) {
                var parsers = [
                    rangeVar_1.RangeVar.fromJs
                ];
                return list.map(function (member) { return parsers.reduce(function (prev, cur) { return prev.isRight() ? prev : cur(member); }, funfix_core_1.Either.left(new Error('A FromClause member is not a FromClauseMember'))); })
                    .reduce(function (prev, cur) {
                    return prev.flatMap(function (members) { return cur.map(function (member) { return members.concat(member); }); });
                }, funfix_core_1.Either.right([]));
            })
                .map(function (members) { return new FromClause(members); });
        };
        return FromClause;
    }());
    exports.FromClause = FromClause;
});
//# sourceMappingURL=fromClause.js.map