(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var Alias = /** @class */ (function () {
        function Alias(name) {
            this.name = name;
        }
        /**
         *
         * @param js { Alias: { aliasname: string } }
         */
        Alias.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.Alias && js.Alias.aliasname; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Missing Alias.aliasname field')); }, function (name) { return funfix_core_1.Either.right(new Alias(name)); });
        };
        return Alias;
    }());
    exports.Alias = Alias;
});
//# sourceMappingURL=alias.js.map