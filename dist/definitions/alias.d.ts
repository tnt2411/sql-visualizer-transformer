import { Either } from 'funfix-core';
export declare class Alias {
    name: string;
    /**
     *
     * @param js { Alias: { aliasname: string } }
     */
    static fromJs(js: any): Either<Error, Alias>;
    constructor(name: string);
}
