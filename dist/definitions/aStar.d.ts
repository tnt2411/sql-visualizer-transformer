import { Either } from 'funfix-core';
export declare class AStar {
    /**
     *
     * @param js {"A_Star":{}}
     */
    static fromJs(js: any): Either<Error, AStar>;
    constructor();
}
