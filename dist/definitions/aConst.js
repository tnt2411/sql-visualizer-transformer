(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "funfix-core", "./integerVal"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var funfix_core_1 = require("funfix-core");
    var integerVal_1 = require("./integerVal");
    var AConst = /** @class */ (function () {
        function AConst(value) {
            this.value = value;
        }
        /**
         *
         * @param js { A_Const: { val: IntegerVal; location: number; } }
        }
         */
        AConst.fromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.A_Const.val; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty val field')); }, function (_) { return funfix_core_1.Either.right(_); })
                .flatMap(function (_) { return integerVal_1.IntegerVal.fromJs(_); })
                .map(function (_) { return new AConst(_); });
        };
        return AConst;
    }());
    exports.AConst = AConst;
});
//# sourceMappingURL=aConst.js.map