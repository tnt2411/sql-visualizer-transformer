import { StringVal } from './stringVal';
import { AConst } from './aConst';
import { Either } from 'funfix-core';
export declare class FuncCall {
    name: StringVal[];
    args: AConst[];
    /**
     *
     * @param js {
                              'FuncCall': {
                                'funcname': StringVal[],
                                'args': AConst[]
                              }
                            }
     */
    static fromJs(js: any): Either<Error, FuncCall>;
    constructor(name: StringVal[], args: AConst[]);
}
