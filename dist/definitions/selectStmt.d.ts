import { Statement } from './statement';
import { Either, Option } from 'funfix-core';
import { ResTarget } from './resTarget';
import { FromClause } from './fromClause';
import { WithClause } from './withClause';
export declare class SelectStmt extends Statement {
    targetList: ResTarget[];
    fromClause: Option<FromClause>;
    withClause: Option<WithClause>;
    static fromJs(js: any): Either<Error, SelectStmt>;
    private static targetListFromJs(js);
    private static fromClauseFromJs(js);
    private static withClauseFromJs(js);
    constructor(targetList: ResTarget[], fromClause?: Option<FromClause>, withClause?: Option<WithClause>);
}
