import { Either } from 'funfix-core';
import { StringVal } from './stringVal';
import { AStar } from './aStar';
export declare type ColumnRefField = StringVal | AStar;
export declare class ColumnRef {
    fields: ColumnRefField[];
    static fromJs(js: any): Either<Error, ColumnRef>;
    constructor(fields: ColumnRefField[]);
}
