(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./columnRef", "./typeName", "../util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var columnRef_1 = require("./columnRef");
    var typeName_1 = require("./typeName");
    var util_1 = require("../util");
    var TypeCast = /** @class */ (function () {
        function TypeCast(arg, typeName) {
            this.arg = arg;
        }
        /**
         *
         * @param js { TypeCast: { arg: ColumnRef; typeName: TypeName; }}
         */
        TypeCast.fromJs = function (js) {
            return util_1.nonEmpty(function () { return js && js.TypeCast && [js.TypeCast.arg, js.TypeCast.typeName]; })
                .flatMap(function (_a) {
                var arg = _a[0], typeName = _a[1];
                var argE = columnRef_1.ColumnRef.fromJs(arg);
                var typeNamE = typeName_1.TypeName.fromJs(typeName);
                return argE.flatMap(function (a) { return typeNamE.map(function (t) { return new TypeCast(a, t); }); });
            });
        };
        return TypeCast;
    }());
    exports.TypeCast = TypeCast;
});
//# sourceMappingURL=typeCast.js.map