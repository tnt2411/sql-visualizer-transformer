var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./statement", "funfix-core", "./resTarget", "util", "./fromClause", "./withClause"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var statement_1 = require("./statement");
    var funfix_core_1 = require("funfix-core");
    var resTarget_1 = require("./resTarget");
    var util_1 = require("util");
    var fromClause_1 = require("./fromClause");
    var withClause_1 = require("./withClause");
    var SelectStmt = /** @class */ (function (_super) {
        __extends(SelectStmt, _super);
        function SelectStmt(targetList, 
            // op: number,
            fromClause, 
            // groupClause: Option<A_Const[]> = Option.none(),
            // havingClause: Option<HavingClause> = Option.none(),
            // windowClause: Option<WindowClause[]> = Option.none(),
            // sortClause: Option<SortBy[]> = Option.none(),
            withClause) {
            if (fromClause === void 0) { fromClause = funfix_core_1.Option.none(); }
            if (withClause === void 0) { withClause = funfix_core_1.Option.none(); }
            var _this = _super.call(this) || this;
            _this.targetList = targetList;
            _this.fromClause = fromClause;
            _this.withClause = withClause;
            return _this;
        }
        SelectStmt.fromJs = function (js) {
            return SelectStmt.targetListFromJs(js)
                .flatMap(function (targetList) { return SelectStmt.fromClauseFromJs(js)
                .flatMap(function (fromClause) { return SelectStmt.withClauseFromJs(js)
                .map(function (withClause) { return new SelectStmt(targetList, fromClause, withClause); }); }); });
        };
        SelectStmt.targetListFromJs = function (js) {
            return funfix_core_1.Try.of(function () { return js && js.targetList; })
                .fold(function () { return funfix_core_1.Either.left(new Error('Expected nonempty targetList field')); }, function (list) { return funfix_core_1.Either.right(list); })
                .flatMap(function (list) { return util_1.isArray(list) ? funfix_core_1.Either.right(list) : funfix_core_1.Either.left(new Error('Expected targetList to be an array')); })
                .flatMap(function (list) {
                return list.map(function (target) { return resTarget_1.ResTarget.fromJs(target); })
                    .reduce(function (prev, cur) { return prev.flatMap(function (list) { return cur.map(function (c) { return list.concat(c); }); }); }, funfix_core_1.Either.right([]));
            });
        };
        SelectStmt.fromClauseFromJs = function (js) {
            return js.fromClause ? fromClause_1.FromClause.fromJs(js.fromClause).map(function (c) { return funfix_core_1.Option.some(c); }) : funfix_core_1.Either.right(funfix_core_1.Option.none());
        };
        SelectStmt.withClauseFromJs = function (js) {
            return js.withClause ? withClause_1.WithClause.fromJs(js.withClause).map(function (c) { return funfix_core_1.Option.some(c); }) : funfix_core_1.Either.right(funfix_core_1.Option.none());
        };
        return SelectStmt;
    }(statement_1.Statement));
    exports.SelectStmt = SelectStmt;
});
//# sourceMappingURL=selectStmt.js.map