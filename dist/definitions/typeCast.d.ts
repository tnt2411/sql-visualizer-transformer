import { Either } from 'funfix-core';
import { ColumnRef } from './columnRef';
import { TypeName } from './typeName';
export declare class TypeCast {
    arg: ColumnRef;
    /**
     *
     * @param js { TypeCast: { arg: ColumnRef; typeName: TypeName; }}
     */
    static fromJs(js: any): Either<Error, TypeCast>;
    constructor(arg: ColumnRef, typeName: TypeName);
}
